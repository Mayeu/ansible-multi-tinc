# Multiple Tinc VPN

This role is dedicated to manage multiple Tinc VPN with overlapping (or not)
host in them.

The canonical repository and issue reporting is located at [OpenYurt's
GitLab](https://code.yourt.es/OpenYurt/ansible-multi-tinc).

## Support & Dependencies

The role only support Ubuntu 16.04, 18.04 and `systemd` as a init system. There
is no plan to support anything else.

You will need to have the python package `netaddr` installed on the ansible
controller.

## Using this role

In the following I will describe the setup used in the test of this playbook
(you can run it with `vagrant up`).

This example is composed of three VPNs:
- `net1` is a set of server on one corner of the internet, using the `10.10.1.0/24` network
- `net3` is another set of server on one corner of the internet, using the `10.10.2.0/24` network
- `net2` is a link between those two network, using the `10.10.0.0/16` network encompassing the two last

Most of the host on `net1` don't know about `net3`'s host, and it is true the
other way around. But some host should be able to access all the other host.

This role expect that all the host using tinc will go into the `tinc` group.
Then you'll have to define the various network you plan to use as groups named
`tinc-<netname>`:

```ini
[tinc]
server-a
server-b
server-c
server-d
server-e

[tinc-net1]
server-a
server-b
server-c

[tinc-net2]
server-c
server-d

[tinc-net3]
server-d
server-e
```

Next, we have to add a variable in our playbook that define which port this
network will use (if no port is defined, the default `655` is used):

```yaml
mtinc_networks:
  - name: net1
    port: 6001
  - name: net2
    port: 655
  - name: net3
    port: 6001
```

Now, we define the IP (with the network netmask) in each host variable (I'll
love this part to be fully handfree, if you have an idea about how robustly
selecting non clashing IP that won't require much external dependencies, plus
[contact me](mailto:m@mayeu.me)!)

Each host should have one IP defined by network it is in. The format should of
the variable must be `mtinc-<netname>-ip`.

An host that has an entire subnetwork behind him, can define a variable named
`mtinc-<netname>-host-subnet`.

Finally, an host can also define a set of additional route to register, via
`mtinc-<netname>-additional-route`.

```yaml
# host_vars/server-a.yml
---
mtinc-net1-ip: "10.10.1.3/24"

# host_vars/server-b.yml
---
mtinc-net1-ip: "10.10.1.2/24"

mtinc-net1-additional-route:
  - net: "10.10.2.0/24"
    gw: "10.10.1.1"

# host_vars/server-c.yml
---
mtinc-net1-ip: "10.10.1.1/24"
# from the perspective of net1, the subnet behind me is:
mtinc-net1-host-subnet: "10.10.0.0/16"

mtinc-net2-ip: "10.10.1.1/16"
# from the perspective of net2, the subnet behind me is:
mtinc-net2-host-subnet: "10.10.1.0/24"

# host_vars/server-d.yml
---
mtinc-net2-ip: "10.10.2.1/16"
# from the perspective of net2, the subnet behind me is:
mtinc-net2-host-subnet: "10.10.2.0/24"

mtinc-net3-ip: "10.10.2.1/24"
# from the perspective of net3, the subnet behind me is:
mtinc-net3-host-subnet: "10.10.0.0/16"

# host_vars/server-e.yml
---
mtinc-net3-ip: "10.10.2.2/24"

mtinc-net3-additional-route:
  - net: "10.10.1.0/24"
    gw: "10.10.2.1"

```

Finally, the playbook should look like this:
```yaml
---
- hosts: tinc
  become: yes
  roles:
    - role: multi-tinc
```

## Other role variables

* `mtinc_configuration_folder`: default to `/etc/tinc`
* `mtinc_networks`: define all the networks you want to create. Default to []
* `mtinc_hostname`: the hostname to use as a tinc hostname
* `mtinc_public_interface`: which interface to get the public IP from
* `mtinc_systemd_use_service_instance:` set to true if the os supports multiple tinc service instances via systemd templates (E.g., Ubuntu 18:04 and better).
* `mtinc_option_tunnel_server`: control the `TunnelServer` option, default to `yes`
  * If activated (`yes`), one host in the network can only reached the hosts it explecitely connects to (and there subnetwork)
  * If deactivated (`no`), an host can connect to any other hosts in the network

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
