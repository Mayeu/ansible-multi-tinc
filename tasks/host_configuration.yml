---
- name: configure the address of the node
  lineinfile:
    path: "{{ mtinc_configuration_folder }}/{{ item.name }}/hosts/{{ item.name }}_{{ mtinc_hostname }}"
    regexp: "^Address="
    line: "Address={{ hostvars[inventory_hostname]['ansible_' + mtinc_public_interface]['ipv4']['address'] }}"
    state: present
    create: yes
    owner: root
    group: root
  when: inventory_hostname in groups['tinc-' + item.name]
  with_items: "{{ mtinc_networks }}"
  notify: restart tinc

- name: configure the port of the node
  lineinfile:
    path: "{{ mtinc_configuration_folder }}/{{ item.name }}/hosts/{{ item.name }}_{{ mtinc_hostname }}"
    regexp: "^Port="
    line: "Port={{ item.port | default(655) }}"
    state: present
    create: yes
    owner: root
    group: root
  when: inventory_hostname in groups['tinc-' + item.name]
  with_items: "{{ mtinc_networks }}"
  notify: restart tinc

- name: configure the compression of the stream
  lineinfile:
    path: "{{ mtinc_configuration_folder }}/{{ item.name }}/hosts/{{ item.name }}_{{ mtinc_hostname }}"
    regexp: "^Compression="
    line: "Compression=0"
    state: present
    create: yes
    owner: root
    group: root
  when: inventory_hostname in groups['tinc-' + item.name]
  with_items: "{{ mtinc_networks }}"
  notify: restart tinc

- name: configure the subnet if the node is the only host behind the VPN
  lineinfile:
    path: "{{ mtinc_configuration_folder }}/{{ item.name }}/hosts/{{ item.name }}_{{ mtinc_hostname }}"
    regexp: "^Subnet="
    line: "Subnet={{ hostvars[inventory_hostname]['mtinc-' + item.name + '-ip'] | ipv4('address')}}/32"
    state: present
    create: yes
    owner: root
    group: root
  when: (inventory_hostname in groups['tinc-' + item.name]) and (hostvars[inventory_hostname]['mtinc-' + item.name + '-host-subnet'] is undefined)
  with_items: "{{ mtinc_networks }}"
  notify: restart tinc

- name: configure the subnet if the node have a full network behind it
  lineinfile:
    path: "{{ mtinc_configuration_folder }}/{{ item.name }}/hosts/{{ item.name }}_{{ mtinc_hostname }}"
    regexp: "^Subnet="
    line: "Subnet={{ hostvars[inventory_hostname]['mtinc-' + item.name + '-host-subnet'] }}"
    state: present
    create: yes
    owner: root
    group: root
  when: (inventory_hostname in groups['tinc-' + item.name]) and (hostvars[inventory_hostname]['mtinc-' + item.name + '-host-subnet'] is defined)
  with_items: "{{ mtinc_networks }}"
  notify: restart tinc

- name: Ensure IP forwarding is activated for node having subnet behind them
  command: sysctl -w net.ipv4.ip_forward=1
  when: (inventory_hostname in groups['tinc-' + item.name]) and (hostvars[inventory_hostname]['mtinc-' + item.name + '-host-subnet'] is defined)
  with_items: "{{ mtinc_networks }}"
  changed_when: false

- name: Ensure IP forwarding survive reboot having subnet behind them
  lineinfile:
    path: /etc/sysctl.conf
    create: yes
    regexp: "^net.ipv4.ip_forward"
    line: "net.ipv4.ip_forward=1"
  when: (inventory_hostname in groups['tinc-' + item.name]) and (hostvars[inventory_hostname]['mtinc-' + item.name + '-host-subnet'] is defined)
  with_items: "{{ mtinc_networks }}"

- name: add a notice that the host file is managed by Ansible
  lineinfile:
    path: "{{ mtinc_configuration_folder }}/{{ item.name }}/hosts/{{ item.name }}_{{ mtinc_hostname }}"
    regexp: "^# This files is managed by Ansible. Any change you made will be overwritten"
    line: "# This files is managed by Ansible. Any change you made will be overwritten"
    insertbefore: BOF
    state: present
    create: yes
    owner: root
    group: root
  when: inventory_hostname in groups['tinc-' + item.name]
  with_items: "{{ mtinc_networks }}"
  notify: restart tinc

