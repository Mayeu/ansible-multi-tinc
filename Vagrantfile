# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "ubuntu/xenial64"

  N = 4
  ('a'..'e').each_with_index do |name, index|
    config.vm.define "server-#{name}" do |server|
      server.vm.hostname = "server-#{name}"
      server.vm.network "private_network", ip: "10.0.5.#{index + 2}"

      # Only execute once the Ansible provisioner,
      # when all the machines are up and ready.
      if index == N
        server.vm.provision :ansible do |ansible|
          # Disable default limit to connect to all the machines
          ansible.limit = "all"
          ansible.playbook = "tests/playbook.yml"
          ansible.raw_arguments = ['-f 5']
          ansible.groups = {
              "tinc" => ["server-a", "server-b", "server-c", "server-d", "server-e"],
              "tinc-net1" => ["server-a", "server-b", "server-c"],
              "tinc-net2" => ["server-c", "server-d"],
              "tinc-net3" => ["server-d", "server-e"],
          }
          ansible.host_vars = {
            "server-a" => {"mtinc-net1-ip" => "10.10.1.3/24"},
            "server-b" => {"mtinc-net1-ip" => "10.10.1.2/24"},
            "server-c" => {"mtinc-net1-ip" => "10.10.1.1/24",
                           "mtinc-net1-host-subnet" => "10.10.0.0/16",
                           "mtinc-net2-ip" => "10.10.1.1/16",
                           "mtinc-net2-host-subnet" => "10.10.1.0/24"},
            "server-d" => {"mtinc-net2-ip" => "10.10.2.1/16",
                           "mtinc-net2-host-subnet" => "10.10.2.0/24",
                           "mtinc-net3-ip" => "10.10.2.1/24",
                           "mtinc-net3-host-subnet" => "10.10.0.0/16"},
            "server-e" => {"mtinc-net3-ip" => "10.10.2.2/24"}
          }
        end
      end
    end
  end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL
end
